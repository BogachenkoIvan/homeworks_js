/*
1.Прив'яжіть усім інпутам наступну подію - втрата фокусу кожен інпут виводить своє value в параграф з id="test"
focus blur input.*/

const inputs1 = document.querySelectorAll('input');

inputs1.forEach(input => {
  input.addEventListener('blur', e => {
    const length = Number(e.target.dataset.length);
    if (e.target.value.length === length) {
      e.target.style.borderColor = 'green';
    } else {
      e.target.style.borderColor = 'red';
    }
  });
});



/*
2.Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість, то межа інпуту стає зеленою, якщо неправильна – червоною.*/


const inputs2 = document.querySelectorAll("input[data-length]");

inputs2.forEach(input => {
  input.addEventListener("blur", function() {
    if (this.value.length === Number(this.dataset.length)) {
      this.style.borderColor = "green";
    } else {
      this.style.borderColor = "red";
    }
  });
});


/*
4.- При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень
- Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст: . 
- Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
*/

const input = document.getElementById("price-input");
const container = document.getElementById("container");
const errorMessage = document.getElementById("error-message");

input.addEventListener("blur", function() {
  input.style.border = "";
  errorMessage.style.display = "none";
  const price = parseFloat(input.value);
  if (isNaN(price) || price < 0) {
    errorMessage.innerHTML = "Please enter correct price";
    errorMessage.style.display = "block";
    input.style.border = "2px solid red";
    return;
    }
  const display = document.createElement("div");
  display.classList.add("price-display");
  const span = document.createElement("span");
  span.innerHTML = `Price: ${price}`;
  display.appendChild(span);
  const btn = document.createElement("button");
  btn.innerHTML = "X";
  btn.addEventListener("click", function() {
    container.removeChild(display);
  });
  display.appendChild(btn);
  container.appendChild(display);
});
