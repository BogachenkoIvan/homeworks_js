const generateButton = document.getElementById("generate-button");
      const contentDiv = document.getElementById("content");
      generateButton.addEventListener("click", () => {
        
        const header = document.createElement("h1");
        header.id = "header";
        header.innerText = "Згенерувати теги";
        contentDiv.appendChild(header);

        
        for (let i = 0; i < 5; i++) {
          const p = document.createElement("p");
          p.classList.add("tag");
          p.innerText = `Натиснути для додавання контенту`;
          p.addEventListener("click", () => {
            const content = prompt(`Введіть контент для цього тегу`);
            p.innerText = content;
          });
          contentDiv.appendChild(p);
        }
      });