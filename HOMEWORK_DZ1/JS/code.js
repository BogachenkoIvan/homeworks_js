var x = 6;
document.write("x = " + x + "<br>");

var y = 14;
document.write("y = " + y + "<br>");

var z = 4;
document.write("z = " + z + "<br>");


var rez = x+=y-x++*z;
/*Згідно таблиці пріоритетів:
1. x++ - постфіксний інкремент не буде виконуватись, тому що стоїть з правої сторони;
2. Далі пріоритет множення: 6*4=24;
3.Далі по пріоритету функція віднімання 14-24=-10;
4.І остання операція присвоєння 6+-10=-4.
*/
document.write("rez = x+= y - x++ * z" + "<br>");
document.write("rez = " + rez + "<hr color = 'blue'>");




var x = 6;
document.write("x = " + x + "<br>");

var y = 14;
document.write("y = " + y + "<br>");

var z = --x-y*5;
/*Згідно таблиці пріоритетів:
1. --x - префіксний декремент виконується отримуємо 6-1=5.
2. Далі пріоритет множення: 14*5=70;
3.І остання операція віднімання 5-70=-65.
*/
document.write("z = --x - y * 5" + "<br>");
document.write("z = " + z + "<hr color = 'blue'>");




var x = 6;
document.write("x = " + x + "<br>");

var y = 14;
document.write("y = " + y + "<br>");

var z = 4;
document.write("z = " + z + "<br>");


var rez = y/=x+5%z;
/*Згідно таблиці пріоритетів:
1. Перша операція залишок від ділення 5/4=1;
2. Далі пріоритет додавання: 6+1=7;
3.І остання операція присвоєння: 14/7=2.
*/
document.write("rez = y /= x + 5 % z" + "<br>");
document.write("rez = " + rez + "<hr color = 'blue'>");





var x = 6;
document.write("x = " + x + "<br>");

var y = 14;
document.write("y = " + y + "<br>");

var z = 4;
document.write("z = " + z + "<br>");


var rez = z-x++ + y * 5;
/*Згідно таблиці пріоритетів:
1. x++ - постфіксний інкремент не буде виконуватись, тому що стоїть з правої сторони;
2. Далі пріоритет функція множення: 14*5=70;
3.Далі по пріоритету функція додавання -6+70=64;
4.І остання операція присвоєння 4+64=68.
*/
document.write("rez = z - x++ + y * 5" + "<br>");
document.write("rez = " + rez + "<hr color = 'blue'>");



var x = 6;
document.write("x = " + x + "<br>");

var z = 4;
document.write("z = " + z + "<br>");

var y = 14;
document.write("y = " + y + "<br>");

var rez = y-x++*z;
/*Згідно таблиці пріоритетів:
1. x++ - постфіксний інкремент не буде виконуватись, тому що стоїть з правої сторони;
2. Далі пріоритет функція множення: 6*4=24;
3.Далі по пріоритету функція віднімання 14-24=-10.
*/
document.write("x = y - x++ * z" + "<br>");
document.write("x = " + rez + "<hr color = 'blue'>");