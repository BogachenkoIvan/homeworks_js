const containerRegDiv = document.createElement('div');
containerRegDiv.classList.add('container');
containerRegDiv.classList.add('container_reg');


const blockRegImgDiv = document.createElement('div');
blockRegImgDiv.classList.add('block_img');

const imgReg = document.createElement('img');
imgReg.setAttribute('src', './images/Figure.png');
imgReg.setAttribute('alt', 'page_login');

blockRegImgDiv.appendChild(imgReg);


const blockFormRegDiv = document.createElement('div');
blockFormRegDiv.classList.add('block_form');


const formReg = document.createElement('form');
formReg.setAttribute('id', 'form_reg')

const headingReg = document.createElement('h2');
headingReg.textContent = 'Registration form';

const userNameDiv = document.createElement('div');

const userNameLabel = document.createElement('label');
userNameLabel.setAttribute('for', 'username');
userNameLabel.textContent = 'Name';

const userNameInput = document.createElement('input');
userNameInput.setAttribute('type', 'text');
userNameInput.setAttribute('class', 'username');
userNameInput.setAttribute('id', 'regusername');
userNameInput.setAttribute('name', 'username');

userNameDiv.appendChild(userNameLabel);
userNameDiv.appendChild(userNameInput);



const userDateOfBirthDiv = document.createElement('div');

const userDateOfBirthLabel = document.createElement('label');
userDateOfBirthLabel.setAttribute('for', 'username');
userDateOfBirthLabel.textContent = 'Date Of Birth';

const userDateOfBirthInput = document.createElement('input');
userDateOfBirthInput.setAttribute('class', 'username');
userDateOfBirthInput.setAttribute('id', 'userdateofbirth');
userDateOfBirthInput.setAttribute('type', 'date');
userDateOfBirthInput.setAttribute('name', 'dateofbirth');


userDateOfBirthDiv.appendChild(userDateOfBirthLabel);
userDateOfBirthDiv.appendChild(userDateOfBirthInput);



const userFather_MotherNameDiv = document.createElement('div');


const userFather_MotherNameLabel = document.createElement('label');
userFather_MotherNameLabel.setAttribute('for', 'username');
userFather_MotherNameLabel.textContent = 'Father’s/Mother’s Name';

const userFather_MotherNameInput = document.createElement('input');
userFather_MotherNameInput.setAttribute('type', 'text');
userFather_MotherNameInput.setAttribute('class', 'username');
userFather_MotherNameInput.setAttribute('id', 'fathermothername');
userFather_MotherNameInput.setAttribute('name', 'fathermothername');

userFather_MotherNameDiv.appendChild(userFather_MotherNameLabel);
userFather_MotherNameDiv.appendChild(userFather_MotherNameInput);



const emailRegFormDiv = document.createElement('div');

const emailRegFormLabel = document.createElement('label');
emailRegFormLabel.setAttribute('for', 'username');
emailRegFormLabel.textContent = 'Email';

const emailRegFormInput = document.createElement('input');
emailRegFormInput.setAttribute('type', 'email');
emailRegFormInput.setAttribute('class', 'username');
emailRegFormInput.setAttribute('id', 'email');
emailRegFormInput.setAttribute('name', 'usernamereg');

emailRegFormDiv.appendChild(emailRegFormLabel);
emailRegFormDiv.appendChild(emailRegFormInput);


const userMobileDiv = document.createElement('div');


const userMobileLabel = document.createElement('label');
userMobileLabel.setAttribute('for', 'username');
userMobileLabel.textContent = 'Mobile No.';

const userMobileInput = document.createElement('input');
userMobileInput.setAttribute('class', 'username');
userMobileInput.setAttribute('id', 'mobilenumber');
userMobileInput.setAttribute('name', 'mobilenumber');


userMobileDiv.appendChild(userMobileLabel);
userMobileDiv.appendChild(userMobileInput);


const passRegDiv = document.createElement('div');

const passRegLabel = document.createElement('label');
passRegLabel.setAttribute('for', 'passreg');
passRegLabel.textContent = 'Password (min 8 знаків, 1 загл. літ.)';

const passRegInput = document.createElement('input');
passRegInput.setAttribute('type', 'password');
passRegInput.setAttribute('id', 'passreg');
passRegInput.setAttribute('required', true);

passRegDiv.appendChild(passRegLabel);
passRegDiv.appendChild(passRegInput);


const rePassRegDiv = document.createElement('div');

const rePassRegLabel = document.createElement('label');
rePassRegLabel.setAttribute('for', 'repassreg');
rePassRegLabel.textContent = 'Re-enter Password';

const rePassRegInput = document.createElement('input');
rePassRegInput.setAttribute('type', 'password');
rePassRegInput.setAttribute('id', 'repassreg');
rePassRegInput.setAttribute('required', true);

rePassRegDiv.appendChild(rePassRegLabel);
rePassRegDiv.appendChild(rePassRegInput);


const userHomeNumberDiv = document.createElement('div');

const userHomeNumberLabel = document.createElement('label');
userHomeNumberLabel.setAttribute('for', 'username');
userHomeNumberLabel.textContent = 'home Number';

const userHomeNumberInput = document.createElement('input');
userHomeNumberInput.setAttribute('class', 'username');
userHomeNumberInput.setAttribute('id', 'homenumber');
userHomeNumberInput.setAttribute('name', 'homenumber');

userHomeNumberDiv.appendChild(userHomeNumberLabel);
userHomeNumberDiv.appendChild(userHomeNumberInput);


const buttonRegDiv = document.createElement('div');
const buttonReg = document.createElement('input');
buttonReg.setAttribute('type', 'submit');
buttonReg.setAttribute('id', 'btn_reg');
buttonReg.classList.add('btn');
buttonReg.setAttribute('value', 'submit');
buttonRegDiv.appendChild(buttonReg);


formReg.appendChild(headingReg);
formReg.appendChild(userNameDiv);
formReg.appendChild(userDateOfBirthDiv);
formReg.appendChild(userFather_MotherNameDiv);
formReg.appendChild(emailRegFormDiv);
formReg.appendChild(userMobileDiv);
formReg.appendChild(passRegDiv);
formReg.appendChild(rePassRegDiv);
formReg.appendChild(userHomeNumberDiv);
formReg.appendChild(buttonRegDiv);


containerRegDiv.appendChild(blockRegImgDiv);
containerRegDiv.appendChild(blockFormRegDiv);

blockFormRegDiv.appendChild(formReg);

document.body.appendChild(containerRegDiv);


document.getElementById('form_reg').addEventListener('submit', (e) => {
    e.preventDefault();

    const nameInput = document.getElementById('regusername');
    const emailInput = document.getElementById('email');
    const passwordInput = document.getElementById('passreg');


    const nameRegex = /^[a-zA-Zа-яА-Я]+$/;
    const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;

 

    const nameValue = nameInput.value.trim();
    const emailValue = emailInput.value.trim();
    const passwordValue = passwordInput.value.trim();
    const rePasswordValue = rePassRegInput.value.trim();
  

  if (!nameValue.match(nameRegex)) {
    nameInput.style.borderColor = 'red';
    return alert('Будь ласка введіть коректне імя');
  }

  if (!emailValue.match(emailRegex)) {
    emailInput.style.borderColor = 'red';
    return alert('Будь ласка введіть коректну email адресу');
  }

  if (!passwordValue.match(passwordRegex)) {
    passwordInput.style.borderColor = 'red';
    return alert('Будь ласка введіть коректний пароль');
  }
    
    if (passwordValue !== rePasswordValue) {
        rePassRegInput.style.borderColor = 'red';
        return alert('Паролі повинні співпадати');
    }

    
    function checkUserExists(username) {
      const users = JSON.parse(localStorage.getItem("users")) || [];
      return users.some((user) => user.username === username);
    }

    const username = document.getElementById("regusername" || "email").value;
      if (checkUserExists(username)) {
        alert("Користувач вже існує");
        return;
      }

  const name = document.getElementById('regusername').value;
  const dateOfBirth = document.getElementById('userdateofbirth').value;
  const fatherMotherName = document.getElementById('fathermothername').value;
  const email = document.getElementById('email').value;
  const mobileNumber = document.getElementById('mobilenumber').value;
  const homeNumber = document.getElementById('homenumber').value;
  const password = document.getElementById('passreg').value;

  const userData = {
    name,
    dateOfBirth,
    fatherMotherName,
    email,
    mobileNumber,
    homeNumber,
    password
  };

  const users = JSON.parse(localStorage.getItem("users")) || [];
  users.push(userData);
  localStorage.setItem("users", JSON.stringify(users));
  
  formReg.reset();

  alert('Ви успішно зареєструвались!');

});









