const containerDiv = document.createElement('div');
containerDiv.classList.add('container');


const blockImgDiv = document.createElement('div');
blockImgDiv.classList.add('block_img');

const img = document.createElement('img');
img.setAttribute('src', './images/login 1.png');
img.setAttribute('alt', 'page_login');

blockImgDiv.appendChild(img);


const blockFormDiv = document.createElement('div');
blockFormDiv.classList.add('block_form');


const heading = document.createElement('h2');
heading.textContent = 'Login';

const form = document.createElement('form');

const emailDiv = document.createElement('div');


const emailLabel = document.createElement('label');
emailLabel.setAttribute('for', 'username');
emailLabel.textContent = 'Email';

const emailInput = document.createElement('input');
emailInput.setAttribute('type', 'email');
emailInput.setAttribute('class', 'username');
emailInput.setAttribute('name', 'username');
emailInput.addEventListener('blur', validateEmail);


emailDiv.appendChild(emailLabel);
emailDiv.appendChild(emailInput);


const passDiv = document.createElement('div');


const passLabel = document.createElement('label');
passLabel.setAttribute('for', 'pass');
passLabel.textContent = 'Password';


const passInput = document.createElement('input');
passInput.setAttribute('type', 'password');
passInput.setAttribute('id', 'pass');
passInput.setAttribute('name', 'password');
passInput.setAttribute('required', true);
passInput.addEventListener('blur', validatePassword);


passDiv.appendChild(passLabel);
passDiv.appendChild(passInput);


const registerPara = document.createElement('p');
const registerSpan = document.createElement('span');
registerSpan.textContent = 'Not a user?';
const registerLink = document.createElement('a');
registerLink.setAttribute('href', './reg_form.html');
registerLink.textContent = 'Register now';
registerPara.appendChild(registerSpan);
registerPara.appendChild(registerLink);


const buttonDiv = document.createElement('div');
const button = document.createElement('input');
button.setAttribute('type', 'button');
button.classList.add('btn');
button.setAttribute('value', 'Login');
buttonDiv.appendChild(button);
button.addEventListener('click', authorizeUser);


form.appendChild(heading);
form.appendChild(emailDiv);
form.appendChild(passDiv);
form.appendChild(registerPara);
form.appendChild(buttonDiv);


containerDiv.appendChild(blockImgDiv);
containerDiv.appendChild(blockFormDiv);

blockFormDiv.appendChild(form);

document.body.appendChild(containerDiv);





function validateEmail() {
    const email = emailInput.value;
    const regex = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    if (regex.test(email)) {
      emailInput.style.borderColor = '#ced4da';
      return true;
    } else {
      emailInput.style.borderColor = 'red';
      return false;
    }
  }
  
  
  function validatePassword() {
    const pass = passInput.value;
    if (pass.trim() !== '') {
      passInput.style.borderColor = '#ced4da';
      return true;
    } else {
      passInput.style.borderColor = 'red';
      return false;
    }
  }
  
  
  function authorizeUser() {
    const email = emailInput.value;
    const pass = passInput.value;
    if (validateEmail() && validatePassword()) {
      const users = JSON.parse(localStorage.getItem('users')) || [];
      const user = users.find((u) => u.email === email && u.password === pass);
      if (user) {
        window.location.href = './hello_page.html';
      } else {
        alert('Невірний email або пароль. Можливо користувач не зареєстрований!');
      }
    } else {
      alert('Будь ласка заповніть усі поля!');
    }
  }



