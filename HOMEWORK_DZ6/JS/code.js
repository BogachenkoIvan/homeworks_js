// Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище),
// rate (ставка за день роботи), days (кількість відпрацьованих днів).
// Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
// Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.

class Worker {
    constructor(name, surname, rate, days) {
        this.name = name;
        this.surname = surname;
        this.rate = rate;
        this.days = days; 
    }
    getSalary() {
         return this.days * this.rate;
    }
}

const workerOne = new Worker("Petr", "Petrov", 22, 800);
console.log(workerOne.name);
console.log(workerOne.surname);
console.log(workerOne.rate);
console.log(workerOne.days);
console.log(workerOne.getSalary());



// Реалізуйте клас MyString, який матиме такі методи: метод reverse(),
// який параметром приймає рядок, а повертає її в перевернутому вигляді, метод ucFirst(),
// який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою
// та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.


class MyString {
    constructor(str) {
        this.str = str;
        this.lastElem = this.str.length-1;
        this.reversStr = " ";
        this.arr = this.str.split(" ");
        this.strArr = [];
    }
    reverse() {
        for (let i =  this.lastElem; i >= 0; i--) {
            this.reversStr += this.str[i];
        }
        return this.reversStr;
    }

    ucFirst() {
        return this.str[0].toUpperCase() + this.str.slice(1);
    }

    ucWords() { 
        for (let i = 0; i < this.arr.length; i++) {
            let y = this.arr[i];
            let newStr = y[0].toUpperCase() + y.slice(1);
                this.strArr.push(newStr);
        }
            return this.strArr.join(" ");
    }

}

const str = new MyString("my name is ivan");

console.log(str.reverse());
console.log(str.ucFirst());
console.log(str.ucWords());




// Створіть клас Phone, який містить змінні number, model і weight.
// Створіть три екземпляри цього класу.
// Виведіть на консоль значення їх змінних.
// Додати в клас Phone методи: receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}". Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів.


class Phone {
    constructor(number, model, weight) {
        this.number = number;
        this.model = model;
        this.weight = weight;
    }

    receiveCall(name) {
        return console.log(`Телефонує: ${name}`);
    }

    getNumber() {
        return console.log (this.number);
    }

}

const iphone = new Phone ("+380990000001", "IPHONE", "200гр.");
const xiaomi = new Phone ("+380990000002", "Xiaomi", "250гр.");
const lg = new Phone ("+380990000003", "LG", "230гр.");

console.log(iphone);
console.log(xiaomi);
console.log(lg);
iphone.receiveCall("Petr");
xiaomi.receiveCall("Alex");
lg.receiveCall("Ivan");
iphone.getNumber();
xiaomi.getNumber();
lg.getNumber();


// Створити клас Car , Engine та Driver.
// Клас Driver містить поля - ПІБ, стаж водіння.
// Клас Engine містить поля – потужність, виробник.
// Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

// Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
// Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.


class Driver {
    constructor(name, experience) {
        this.name = name;
        this.experience = experience;
    }
}

class Engine {
    constructor(power, manufacturer) {
        this.power = power;
        this.manufacturer = manufacturer;
    }
}

class Car {
    constructor(brand, carClass, weight, driver, engine) {
        this.brand = brand;
        this.carClass = carClass;
        this.weight = weight;
        this.driver = driver;
        this.engine = engine;
    }

    start() {
        return console.log("Поїхали");
    }

    stop() {
        return console.log("Зупиняємося");
    }

    turnRight() {
        return console.log("Поворот праворуч");
    }

    turnLeft() {
        return console.log("Поворот ліворуч");
    }

    toString() {
        return console.log(this);
    }
}

const driver = new Driver ("Ivanov Ivan Ivanovich", "20 years");
const engine = new Engine ("250hp", "BMW");
const car = new Car ("BMW", "sedan", "2200kg", driver, engine);

car.toString();

class Lorry extends Car {
    constructor (brand, carClass, weight, driver, engine, capacity) {
        super(brand, carClass, weight, driver, engine);
        this.capacity = capacity;
    }
}


class SportCar extends Car {
    constructor (brand, carClass, weight, driver, engine, limSpeed) {
        super(brand, carClass, weight, driver, engine);
        this.limSpeed = limSpeed;
    }
}

const car2 = new SportCar ("Lamborghini", "sportcar", "2200kg", driver, engine, 300);

car2.toString();






