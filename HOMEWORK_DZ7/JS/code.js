// Написати функцію `filterBy()`, яка повертатиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних. Функція повинна повернути новий масив, який міститиме всі дані, які були предані в аргумент, за вийнятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].  

let array = ['hello', 'world', 23, '23', null];

function filterBy (arr, type) {
    return arr.filter(item => typeof item !== type);
} 

console.log(filterBy(array, "string"));




const words = [
    "программа",
    "макака",
    "прекрасный",
    "оладушек"
    ];

    // Выбираем случайное слово
    let word = words[Math.floor(Math.random() * words.length)];
    // Создаем итоговый массив
    const answerArray = [];
    for (let i = 0; i < word.length; i++) {
    answerArray[i] = "_";
    }
    let remainingLetters = word.length;
    // Игровой цикл
    while (remainingLetters > 0) {
    // Показываем состояние игры
    alert(answerArray.join(" "));
//    Hangman — 
//    здесь «Виселица»
//    7. Пишем игру «Виселица» 123
    // Запрашиваем вариант ответа
    let guess = prompt(`Угадайте букву, или нажмите Отмена для выхода из игры.`);
    if (guess === null) {
    // Выходим из игрового цикла
    break;
    } else if (guess.length !== 1) {
    alert(`Пожалуйста, введите одиночную букву.`);
    } else {
    // Обновляем состояние игры
    for (let j = 0; j < word.length; j++) {
    if (word[j] === guess) {
    answerArray[j] = guess;
    remainingLetters--;
    }
    }
    }
    // Конец игрового цикла
    }
    // Отображаем ответ и поздравляем игрока
    alert(answerArray.join(" "));
    alert(`Отлично! Было загадано слово ${word}`);


    
    
   