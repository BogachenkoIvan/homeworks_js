import pizza from "./pizza.js";

const pizzaSelectUser = {
    size : "",
    topping : [],
    sauce : [],
    price : 0
 }

function userSlectTopping(topping) {
   
    if ("smallmidbig".includes(topping)) {
        pizzaSelectUser.size = pizza.size.find((el) => {
            return el.name === topping;
        });
    } else if ("moc1moc2moc3telyavetch1vetch2".includes(topping)) {
        pizzaSelectUser.topping.push(pizza.topping.find(el => el.name === topping));
        document.querySelector('#topping').append(createDiv(render(pizzaSelectUser.topping)));

    } else if ("sauceClassicsauceBBQsauceRikotta".includes(topping)) {
        pizzaSelectUser.sauce.push(pizza.sauce.find(el => el.name === topping));
        document.querySelector('#sauce').append(createDiv(render(pizzaSelectUser.sauce)));
    }
    pizzaSelectUser.price = show(pizzaSelectUser);
 

    document.querySelector('#price').innerHTML = `${pizzaSelectUser.price} грн.`;
   
}
function removeButton(topping) {
    
    if ("Сир звичайнийСир фетаМоцареллаТелятинаПомiдориГриби".includes(topping)) {
        pizzaSelectUser.topping.splice(pizzaSelectUser.topping.indexOf(el => el.productName === topping), 1);

    } else if ("КетчупБарбекюРикота".includes(topping)) {
        pizzaSelectUser.sauce.splice(pizzaSelectUser.sauce.indexOf(el => el.productName === topping), 1);
    
    }
    pizzaSelectUser.price = show(pizzaSelectUser);
 

    document.querySelector('#price').innerHTML = `${pizzaSelectUser.price} грн.`;
   
}

function createDiv(el) {
    
    let div = document.createElement("div");
    let span = document.createElement("span"); 
    let buttonRemove = document.createElement("button");
    buttonRemove.textContent = "Видалити";
    span.textContent = el;
    buttonRemove.setAttribute('class','remove');
    div.append(span);
    div.append(buttonRemove);
    return div;
}

function removeIngredients(ingred) {
    ingred.parentNode.remove();
}


function show(pizza) {
    let price = 0;
    if (pizza.sauce.length > 0) {
        price += pizza.sauce.reduce((a,b)=>{
            return a + b.price;
        }, 0);
    }
    if(pizza.topping.length > 0){
        price += pizza.topping.reduce((a,b)=>{
            return a + b.price;
        }, 0);
    }
    if(pizza.size !== ""){
        price += pizza.size.price;
    }
    return price;
}


function render(obj) {
    let names = '';
    obj.forEach((el => names=el.productName));
    return names;
}



export { userSlectTopping, pizzaSelectUser, removeIngredients, removeButton };




