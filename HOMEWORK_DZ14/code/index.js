import {clickInputSize, clickSauceAdd, clickToppingAdd, removeIngredient} from "./functionEvent.js";
import {pizzaSelectUser} from './functions.js';

document.getElementById("pizza")
    .addEventListener("click", clickInputSize);

document.querySelectorAll(".topping")
.forEach((div)=>{
    div.addEventListener("click", clickToppingAdd);
});

document.querySelectorAll(".sauce")
.forEach((div)=>{
    div.addEventListener("click", clickSauceAdd);
});


document.querySelectorAll(".js_sauce")
.forEach((div)=>{
    div.addEventListener("click", removeIngredient);
});

document.querySelectorAll(".js_topping")
.forEach((div)=>{
    div.addEventListener("click", removeIngredient);
});


const banner = document.getElementById("banner");

banner.addEventListener("mouseenter", (event) => {
  const width = banner.offsetWidth;
  const height = banner.offsetHeight;
  const maxX = window.innerWidth - width;
  const maxY = window.innerHeight - height;
  
  banner.style.left = `${Math.floor(Math.random() * maxX)}px`;
  banner.style.top = `${Math.floor(Math.random() * maxY)}px`;
});



const form = document.getElementById("info");
const nameInput = form.elements.name;
const phoneInput = form.elements.phone;
const emailInput = form.elements.email;
const resetBtn = form.elements.cancel;
const submitBtn = form.querySelector("#btnSubmit");

submitBtn.addEventListener("click", (event) => {
  event.preventDefault();
  const name = nameInput.value.trim();
  const phone = phoneInput.value.trim();
  const email = emailInput.value.trim();

  let isValid = true;

  if (!/^[А-ЯІЇЄ][а-яіїє]*$/.test(name)) {
    nameInput.style.border = "1px solid red";
    isValid = false;
  } else {
    nameInput.style.border = "1px solid #ccc";
  }

  if (!/^\+380\d{9}$/.test(phone)) {
    phoneInput.style.border = "1px solid red";
    isValid = false;
  } else {
    phoneInput.style.border = "1px solid #ccc";
  }

  if (!/^\w+@\w+\.\w{2,}$/.test(email)) {
    emailInput.style.border = "1px solid red";
    isValid = false;
  } else {
    emailInput.style.border = "1px solid #ccc";
  }

  if (isValid) {    

    document.getElementById("btnSubmit").addEventListener("click", function(){
      window.location.href = "./thank-you/thanks.html";
    });

  };   
  
});

resetBtn.addEventListener("click", (event) => {
  form.reset();
});

