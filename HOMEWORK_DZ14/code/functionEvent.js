import {userSlectTopping, removeIngredients, removeButton} from "./functions.js";

export function clickInputSize(e) {
    if(e.target.tagName === "INPUT"){
        userSlectTopping(e.target.value)
    }
}

export function clickToppingAdd(e) {
  if (e.target.tagName === "IMG") {
    userSlectTopping(e.target.id)
  }
}

export const clickSauceAdd = (e)=> {
    if(e.target.tagName === "IMG"){
        userSlectTopping(e.target.id)
    }
}

export const removeIngredient = (e)=> {
  removeIngredients(e.target);
  removeButton(e.target.previousElementSibling.innerText);
};


  const saucesList = document.querySelectorAll('.sauce');
  const toppingsList = document.querySelectorAll('.topping');
  const table = document.querySelector('.table');
  
  for (const sauce of saucesList) {
    sauce.addEventListener('dragstart', (event) => {
        event.dataTransfer.setData('text/plain', event.target.id);
        event.dataTransfer.dropEffect = 'copy';
    });     
  }
  
  for (const topping of toppingsList) {
    topping.addEventListener('dragstart', (event) => {
        event.dataTransfer.setData('text/plain', event.target.id);
        event.dataTransfer.dropEffect = 'copy';
      
    });
  }
  
  table.addEventListener('dragover', (event) => {
    event.preventDefault();
  });
  
  table.addEventListener('drop', (event) => {
    const data = event.dataTransfer.getData('text/plain');
    const source = document.getElementById(data);
    let clone = source.cloneNode(true);
    userSlectTopping(source.id)
    table.appendChild(clone);
  });
  

