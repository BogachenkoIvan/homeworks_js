const drawBtn = document.querySelector("#draw-button");
    drawBtn.addEventListener("click", () => {
      const diameter = prompt("Введіть діаметр кола (ціле число): ");
    
      const circleContainer = document.createElement("div");
            circleContainer.className = 'root';
            circleContainer.style.display = "flex";
            circleContainer.style.flexWrap = "wrap";
            circleContainer.style.margin = "0 auto";
            circleContainer.style.width = "100%";
            circleContainer.style.maxWidth = +diameter * 10 + 'px';

            
        document.body.appendChild(circleContainer);
    
        let rowContainer = document.createElement("div");
        rowContainer.style.display = "flex";
        circleContainer.appendChild(rowContainer);

        for (let i = 0; i < 100; i++) {
            rowContainer = document.createElement("div");
            rowContainer.style.display = "flex";
            circleContainer.appendChild(rowContainer);
          
          const circle = document.createElement("div");
          circle.style.width = `${diameter}px`;
          circle.style.height = `${diameter}px`;
          circle.style.borderRadius = "50%";
          circle.style.backgroundColor = getRandomColor();
          circle.style.display = "inline-block";
          rowContainer.appendChild(circle);

          circle.addEventListener("click", () => {
            circle.remove();
            const circles = Array.from(rowContainer);
            circles.splice(circle, 1);
            circles.forEach((c, index) => {
              c.style.left = `${index * diameter}px`;
            });
          });
        }
      });
    

    function getRandomColor() {
              let letters = "0123456789ABCDEF";
              let color = "#";
              for (let i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
              }
              return color;
            }