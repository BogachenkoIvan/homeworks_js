// задача 2


        
let running = false;
let intervalId;
let time = 0;
  const stopwatch = document.getElementById("stopwatch");
  const startButton = document.getElementById("start-button");
  const stopButton = document.getElementById("stop-button");
  const resetButton = document.getElementById("reset-button");

    startButton.addEventListener("click", () => {
      if (!running) {
        running = true;
        stopwatch.style.backgroundColor = "green";
        intervalId = setInterval(() => {
          time++;
          const seconds = time % 60;
          const minutes = Math.floor(time / 60) % 60;
          const hours = Math.floor(time / 3600);
          stopwatch.textContent = `${hours.toString().padStart(2, "0")}:${minutes.toString().padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`;
          }, 1000);
        }
      });

      stopButton.addEventListener("click", () => {
        if (running) {
          running = false;
          clearInterval(intervalId);
          stopwatch.style.backgroundColor = "red";
        }
      });

      resetButton.addEventListener("click", () => {
        if (!running) {
          time = 0;
          stopwatch.textContent = "00:00:00";
          stopwatch.style.backgroundColor = "gray";
        }
      });
   




      // Задача 3

      const phoneInput = document.getElementById("phone-number");
      const saveButton = document.getElementById("save-button");
      const errorParagraph = document.getElementById("error");

      saveButton.addEventListener("click", function() {
        const phoneNumber = phoneInput.value;
        const phoneNumberRegex = /^\d{3}-\d{3}-\d{2}-\d{2}$/;
        if (phoneNumberRegex.test(phoneNumber)) {
          phoneInput.style.backgroundColor = "green";
          document.location = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
        } else {
          errorParagraph.textContent = "Невірний формат номеру телефона! Будь ласка введіть у форматі: 000-000-00-00";
        }
      });
    


