const slider = document.querySelector("#slider");
const images = document.querySelectorAll("#slider img");
let index = 0;

function changeImage() {
  images.forEach(img => {
    img.classList.remove("active");
  });
  images[index].classList.add("active");
  index++;
  if (index >= images.length) {
    index = 0;
  }
}

setInterval(changeImage, 3000);
