const textarea = document.querySelector('textarea');
const section = document.querySelector('section');


function pugToHtml(pug) {
  
  if (!pug) {
    section.textContent = '';
    return;
  }

  const lines = pug.split('\n');
  let html = '';
  let currentIndentLevel = -1;
  let firstLine = true;
  let tag1 = "";
  for (const line of lines) {
    const indentLevel = line.search(/^[.#>\^a-zA-Z]+$/);

    if (indentLevel === -1) {
      html += '</div>'.repeat(currentIndentLevel + 1);
      continue;
    }
    
    if (firstLine) {
      firstLine = false;
    } else {
      if (indentLevel > currentIndentLevel) {
        html += '<div>';
      } else if (indentLevel < currentIndentLevel) {
        html += '</div>'.repeat(currentIndentLevel - indentLevel);
      }
    }

    currentIndentLevel = indentLevel;

    const tagRegExp = /^(\w+)/;
    const tagMatch = tagRegExp.exec(line);

    const tag = tagMatch  ? tagMatch[1] : 'div';
    tag1 = tag;
    html += `<${tag}`;
 
    const classRegExp = /\.(\w+)/g;
    let classMatch;
    while (classMatch = classRegExp.exec(line)) {
      const className = classMatch[1];
      html += ` class="${className}"`;
    }

    const idRegExp = /\#(\w+)/;
    const idMatch = idRegExp.exec(line);
    if (idMatch) {
      const id = idMatch[1];
      html += ` id="${id}"`;
    }

    const attrsRegExp = /\((.+)\)/;
    const attrsMatch = attrsRegExp.exec(line);
    if (attrsMatch) {
      const attrsString = attrsMatch[1];
      const attrs = attrsString.split(',').map(attr => attr.trim());
      attrs.forEach(attr => {
        const [name, value] = attr.split('=');
        html += ` ${name}="${value}"`;
      });
    }

    const selfClosingRegExp = /\/$/;
    const selfClosingMatch = selfClosingRegExp.test(line);
    if (selfClosingMatch) {
      html += ' />';
      continue;
    }

    html += '>';
    const innerRegExp = /\>(\w+)/g;
    while (innerMatch = innerRegExp.exec(line)) {
      const tagInner = innerMatch  ? innerMatch[1] : 'div';
      if (innerMatch[1] === 'a') {
        html += `\n \u00A0 <a href=""></a> \n`;
      }else {
        html += `\n \u00A0 <${tagInner}></${tagInner}> \n`;
      }
    }   
    
    const textRegExp = /^\w+\s+(.*)/;
    const textMatch = textRegExp.exec(line);
    if (textMatch) {
      const text = textMatch[1];
      html += text;
    }
  }

  html += `</${tag1}>`.repeat(currentIndentLevel + 1);
  const linkRegExp = /\^(\w+)/g;
    while (linkMatch = linkRegExp.exec(lines)) {
      const tagLink = linkMatch  ? linkMatch[1] : 'div';
      if (linkMatch[1] === 'a') {
        html += `\n <a href=""></a>`;
      }else {
        html += `<${tagLink}></${tagLink}>`;
      }
    }
  return html;
}

textarea.addEventListener('input', () => {
  const pug = textarea.value;
  const html = pugToHtml(pug);
  section.textContent = html;
});