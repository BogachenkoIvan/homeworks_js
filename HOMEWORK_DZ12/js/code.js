/*
* У папці calculator дана верстка макета калькулятора. 
* Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/



document.addEventListener('DOMContentLoaded', function() {
  const display = document.querySelector('#test');
  const buttons = document.querySelectorAll('.button');
  
  let firstValue = null;
  let operator = null;
  let memory = null;
  
  buttons.forEach(button => {
    button.addEventListener('click', e => {
      const value = e.target.value;
      
      if (value === 'C') {
        firstValue = null;
        operator = null;
        display.value = '';
      } else if (!isNaN(value)) {
        display.value += value;
      } else if (value === '.') {
        if (!display.value.includes('.')) {
          display.value += '.';
        }
      } else if (['+', '-', '*', '/'].includes(value)) {
        firstValue = parseFloat(display.value);
        operator = value;
        display.value = '';
      } else if (value === 'm+') {
        memory = (memory || 0) + parseFloat(display.value);
        display.value = `m${display.value}`;
      } else if (value === 'm-') {
        memory = (memory || 0) - parseFloat(display.value);
        display.value = `m${display.value}`;
      } else if (value === 'mrc') {
        if (memory) {
          display.value = memory;
        }
        memory = null;
      } else if (value === '=') {
        if (firstValue !== null && operator !== null) {
          const secondValue = parseFloat(display.value);
          if (operator === '+') {
            display.value = firstValue + secondValue;
          } else if (operator === '-') {
            display.value = firstValue - secondValue;
          } else if (operator === '*') {
            display.value = firstValue * secondValue;
          } else if (operator === '/') {
            display.value = firstValue / secondValue;
          }
          firstValue = null;
          operator = null;
        }
      }
    });
  });
});
