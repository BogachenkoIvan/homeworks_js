/*  Створіть сайт з коментарями. Коментарі тут : https://jsonplaceholder.typicode.com/
    Сайт має виглядати так : https://kondrashov.online/images/screens/120.png
    На сторінку виводити по 10 коментарів, знизу сторінки зробити поле пагінації (перемикання сторінок) при перемиканні
    сторінок показувати нові коментарі. 
    з коментарів виводити : 
    "id": 
    "name":
    "email":
    "body":
*/

const commentsPerPage = 10;
let currentPage = 1;

const apiUrl = 'https://jsonplaceholder.typicode.com/comments';

const commentsContainer = document.getElementById('comments');
const paginationContainer = document.querySelector('.pagination');

async function getComments(page) {
  const response = await fetch(`${apiUrl}?_page=${page}&_limit=${commentsPerPage}`);
  const data = await response.json();
  return data;
}

function displayComments(comments) {
  commentsContainer.innerHTML = '';
  comments.forEach(comment => {
    const commentElement = `
      <div class="comment">
        <p><strong>Id:</strong> ${comment.id}</p>
        <p><strong>Name:</strong> ${comment.name}</p>
        <p><strong>Email:</strong> ${comment.email}</p>
        <p><strong>Comment:</strong> ${comment.body}</p>
      </div>
    `;
    commentsContainer.innerHTML += commentElement;
  });
}

async function setupPagination() {
  const totalCommentsResponse = await (await fetch(apiUrl)).json();
  const totalComments = totalCommentsResponse.length;
  const totalPages = Math.ceil(totalComments / commentsPerPage);

  console.log(totalCommentsResponse);
  console.log(totalComments);
  console.log(totalPages);

  paginationContainer.innerHTML = '';

  for (let i = 1; i <= totalPages; i++) {
    const button = document.createElement('button');
    button.innerText = i;

    if (i === currentPage) {
      button.classList.add('active');
    }

    button.addEventListener('click', () => {
      currentPage = i;
      getComments(currentPage).then(comments => {
        displayComments(comments);
        updatePagination();
      });
    });

    paginationContainer.appendChild(button);
  }
}

function updatePagination() {
  const buttons = paginationContainer.getElementsByTagName('button');

  for (let i = 0; i < buttons.length; i++) {
    buttons[i].classList.remove('active');
  }

  buttons[currentPage - 1].classList.add('active');

  if (currentPage === 1) {
    buttons[0].classList.add('disabled');
  } else {
    buttons[0].classList.remove('disabled');
  }

  if (currentPage === buttons.length) {
    buttons[buttons.length - 1].classList.add('disabled');
  } else {
    buttons[buttons.length - 1].classList.remove('disabled');
  }
}

getComments(currentPage).then(comments => {
  displayComments(comments);
  setupPagination();
});








